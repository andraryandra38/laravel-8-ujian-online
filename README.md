<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

<h1 class="fw-bold">Selamat Datang di Project Laravel 8 Ujian Online.</h1>

Untuk menggunakan atau menjalankan Project Laravel 8 Ujian Online ini.

1.Pertama download project, kemudian ketikan perintah :
- composer install

2. Ubah nama file .env.example menjadi .env
- copy .env.example .env

3. Ketikan perintah berikut,
- php artisan key:generate

4.lalu siapkan database di localhost/phpmyadmin, untuk nama databasenya bisa dilihat di file .env

5. Jalankan perintah migratation,
- php artisan migrate

6. Lalu jalankan perintah berikut juga,
- php artisan db:seed

7. Kalian tinggal menjalani project ini,
- php artisan serve

8. Buka Browser dan ketikan alamat URL berikut,
- localhost:8000 atau 127.0.0.1:8000

9. Untuk info loginnya sebagai berikut,

<p>SuperAdmin : </p>
<p>Username = superadmin </p>
<p>Password = superadmin</p>

<p>Admin : 
<p>Username = admin </p>
<p>Password = admin</p>

Gunakan project ini sebaik mungkin, dan jadikan sebagai bahan referensi project.
Salam Ngoding Aryand 🥇

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
